import { Component, OnInit, ViewChild, Input, OnDestroy, Output, EventEmitter } from '@angular/core';
import {AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult} from '@universis/ngx-tables';
import { Observable, Subscription } from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import {ActivatedRoute} from '@angular/router';
import {ActivatedTableService} from '@universis/ngx-tables';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import { ErrorService, LoadingService, ModalService, ToastService } from '@universis/common';
import { ClientDataQueryable } from '@themost/client';
import { AngularDataContext } from '@themost/angular';
import { SendMessageActionComponent } from '../send-message-action/send-message-action.component';
import { HttpClient } from '@angular/common/http';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-consulted-students-table',
  templateUrl: './consulted-students-table.component.html',
  styleUrls: ['./consulted-students-table.component.scss']
})
export class ConsultedStudentsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  public selectedItems: any[];
  @Input() tableConfiguration: any;
  @Input() searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  public recordsTotal: any;
  private fragmentSubscription: Subscription;

  constructor(private _activatedRoute: ActivatedRoute,
              private _activatedTable: ActivatedTableService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _modalService: ModalService,
              private _context: AngularDataContext,
              private _toastService: ToastService,
              private _http: HttpClient,
              private _translateService: TranslateService) { }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      try {
        this._activatedTable.activeTable = this.table;
        if (data.searchConfiguration) {
          this.search.form = data.searchConfiguration;
          this.search.ngOnInit();
        }
        if (data.tableConfiguration) {
          // set config
          this.table.config = AdvancedTableConfiguration.cast(data.tableConfiguration);
          // reset search text
          this.advancedSearch.text = null;
          // reset table
          this.table.reset(false);
        }
      } catch (err) {
        this._errorService.navigateToError(err);
      }
    });
  }

  ngOnDestroy() {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }

  onDataLoad(event: AdvancedTableDataResult) {
    this.recordsTotal = event.recordsTotal;
  }

  executeSendDirectMessageAction(message: any) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const total = this.selectedItems.length;
      const result = {
        total: total,
        success: 0,
        errors: 0
      };
      let errors = 0;
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const sendMessage: FormData = new FormData();
            if (message.attachments && Array.isArray(message.attachments) && message.attachments.length === 1) {
              sendMessage.append('file', message.attachments[0], message.attachments[0].name || 'attachment');
            }
            sendMessage.append('body', typeof(message.body) === 'undefined' ? '' : message.body);
            sendMessage.append('subject', typeof(message.subject) === 'undefined' ? '' : message.subject);
            const serviceHeaders = this._context.getService().getHeaders();
            const postUrl = this._context.getService().resolve(`instructors/me/consultedStudents/${item.student}/sendMessage`);
            await this._http.post(postUrl, sendMessage, {
              headers: serviceHeaders
            }).toPromise();
            result.success += 1;
          } catch (err) {
            // log error
            console.log(err);
            errors += 1;
          }

        }
      })().then(() => {
        if (errors === 0) {
          this._toastService.show(this._translateService.instant('ConsultedStudents.SendMessageTitle'),
          this._translateService.instant('ConsultedStudents.SendMessageSuccess'));
        } else {
          const description = errors === 1 ? this._translateService.instant('ConsultedStudents.SendMessageFailureSingular')
            : this._translateService.instant('ConsultedStudents.SendMessageFailure');
          this._toastService.show(this._translateService.instant('ConsultedStudents.SendMessageTitle'),
          `${errors.toString()}  ${description}`);
        }
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }

  async sendDirectMessageAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items.filter(item => item.studentUser != null);
      this._loadingService.hideLoading();
      const message = {};
      this._modalService.openModalComponent(SendMessageActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Messages.SendDirectMessageToCandidate.Title',
          description: 'Messages.SendDirectMessageToCandidate.Description',
          refresh: this.refreshAction,
          message: message,
          execute: this.executeSendDirectMessageAction(message)
        }
      });
    } catch (err) {
      console.error(err);
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'student/id as student', 'student/user as studentUser'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter(item => {
              return this.table.unselected.findIndex((x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map((item) => {
            return {
              id: item.id,
              student: item.student,
              studentUser: item.studentUser
            };
          });
        }
      }
    }
    return items;
  }
}

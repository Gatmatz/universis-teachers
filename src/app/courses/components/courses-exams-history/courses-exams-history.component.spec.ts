import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CoursesExamsHistoryComponent } from './courses-exams-history.component';

describe('CoursesExamsHistoryComponent', () => {
  let component: CoursesExamsHistoryComponent;
  let fixture: ComponentFixture<CoursesExamsHistoryComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CoursesExamsHistoryComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CoursesExamsHistoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import {Model} from '@universis/ngx-events';
import { DiagnosticsService } from '@universis/common';
@Component({
  templateUrl: 'dashboard.component.html'
})
export class DashboardComponent implements OnInit {

  model = Model.Instructor;
  public supportQA = false;

  constructor(private _diagnosticsService: DiagnosticsService) { }

  ngOnInit(): void {
    this._diagnosticsService.hasService('QualityAssuranceService').then((result) => {
      this.supportQA = result;
    }).catch(err => {
      console.log(err);
    });
  }

}
